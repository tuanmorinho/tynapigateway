FROM openjdk:20
COPY target/tyngateway-0.0.1-SNAPSHOT.jar tyngateway-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java","-jar","/tyngateway-0.0.1-SNAPSHOT.jar"]