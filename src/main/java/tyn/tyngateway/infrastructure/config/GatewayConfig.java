package tyn.tyngateway.infrastructure.config;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import tyn.tyngateway.infrastructure.config.filter.AuthenticationFilter;

@Configuration
@AllArgsConstructor
public class GatewayConfig  {

    @Autowired
    private final AuthenticationFilter authFilter;

    @Bean
    public RouteLocator routes(RouteLocatorBuilder builder) {
        return builder.routes()
                .route("master-service", r -> r.path("/api/master/v1/**")
                        .filters(f -> f.rewritePath("/api/master/v1/(?<remaining>.*)", "/$\\{remaining}").filter(authFilter))
                        .uri("lb://master-service"))

                .route("storage-service", r -> r.path("/api/storage/v1/**")
                        .filters(f -> f.rewritePath("/api/storage/v1/(?<remaining>.*)", "/$\\{remaining}").filter(authFilter))
                        .uri("lb://storage-service"))

                .route("movie-service", r -> r.path("/api/movie/v1/**")
                        .filters(f -> f.rewritePath("/api/movie/v1/(?<remaining>.*)", "/$\\{remaining}").filter(authFilter))
                        .uri("lb://movie-service"))

                .route("order-service", r -> r.path("/api/order/v1/**")
                        .filters(f -> f.rewritePath("/api/order/v1/(?<remaining>.*)", "/$\\{remaining}").filter(authFilter))
                        .uri("lb://order-service"))
                .build();
    }
}
