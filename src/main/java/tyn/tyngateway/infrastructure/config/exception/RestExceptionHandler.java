package tyn.tyngateway.infrastructure.config.exception;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.reactive.result.method.annotation.ResponseEntityExceptionHandler;
import tyn.tyngateway.app.dto.ApiErrorResponse;
import tyn.tyngateway.infrastructure.core.MessageLocaleService;

@ControllerAdvice
@RequiredArgsConstructor
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

    @Autowired
    private final MessageLocaleService messageLocaleService;

    @ExceptionHandler(RestException.class)
    public ResponseEntity<ApiErrorResponse> handlerServerApiException(RestException restException) {
        final ApiErrorResponse apiErrorResponse = ApiErrorResponse.builder()
                .message(generateMessage(restException.getErrorCode(), restException.getParams()))
                .status(restException.getHttpStatus().value())
                .build();
        return new ResponseEntity<>(apiErrorResponse, restException.getHttpStatus());
    }

    private String generateMessage(String messageId, Object... params) {
        return messageLocaleService.getMessage(messageId, params);
    }
}
