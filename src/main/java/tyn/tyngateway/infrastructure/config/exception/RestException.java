package tyn.tyngateway.infrastructure.config.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
public class RestException extends RuntimeException {

    private final String errorCode;
    private final HttpStatus httpStatus;
    private final Object[] params;

    public RestException(String errorCode, HttpStatus httpStatus, Object... params) {
        this.errorCode = errorCode;
        this.httpStatus = httpStatus;
        this.params = params;
    }
}
