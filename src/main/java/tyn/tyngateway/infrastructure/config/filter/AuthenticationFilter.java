package tyn.tyngateway.infrastructure.config.filter;

import io.jsonwebtoken.Claims;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;
import tyn.tyngateway.infrastructure.config.validator.RouterValidator;
import tyn.tyngateway.infrastructure.utility.JwtUtil;

@RefreshScope
@Component
@RequiredArgsConstructor
public class AuthenticationFilter implements GatewayFilter {

    @Autowired
    private final RouterValidator routerValidator;

    @Autowired
    private final JwtUtil jwtUtil;

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        ServerHttpRequest request = exchange.getRequest();
        if (routerValidator.isSecured.test(request)) {
            if (isAuthMissing(request))
                return onError(exchange);
            final String token = getJwtFromRequest(request);
            if (!StringUtils.hasText(token) || (StringUtils.hasText(token) && jwtUtil.isTokenInValid(token)))
                return onError(exchange);
            populateRequestWithHeaders(exchange, token);
        }
        return chain.filter(exchange);
    }

    private Mono<Void> onError(ServerWebExchange exchange) {
        ServerHttpResponse response = exchange.getResponse();
        response.setStatusCode(HttpStatus.UNAUTHORIZED);
        return response.setComplete();
    }

    private boolean isAuthMissing(ServerHttpRequest request) {
        return !request.getHeaders().containsKey(HttpHeaders.AUTHORIZATION);
    }

    private String getJwtFromRequest(ServerHttpRequest request) {
        final String bearerToken = request.getHeaders().getOrEmpty(HttpHeaders.AUTHORIZATION).get(0);
        if (StringUtils.hasText(bearerToken) && bearerToken.startsWith("Bearer ")) {
            return bearerToken.substring(7);
        }
        return null;
    }

    private void populateRequestWithHeaders(ServerWebExchange exchange, String token) {
        final Claims claims = jwtUtil.extractAllClaims(token);
        exchange.getRequest().mutate()
                .header("username", "Bearer " + claims.get("sub"))
                .header("roles", "Bearer " + claims.get("roles"))
                .build();
    }
}
