package tyn.tyngateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class TyngatewayApplication {

    public static void main(String[] args) {
        SpringApplication.run(TyngatewayApplication.class, args);
    }

}
